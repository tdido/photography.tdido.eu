import glob
import json
import os

from PIL import Image

galleries = {}

# get gallery dirs sorted by date
gdirs = sorted(glob.glob("img/*"), key=lambda x: x.split("_")[-1])

for gdir in gdirs:
    gname = os.path.basename(gdir)
    tmp = []

    # get content from the 'order' files to prioritise certain images, if present
    try:
        with open(f"{gdir}/order") as ifh:
            images = [gdir + "/" + r.rstrip("\n") for r in ifh]
    except FileNotFoundError:
        images = []

    for f in images:
        img = Image.open(f)
        tmp.append({"src": f, "width": img.width, "height": img.height})

    # remove the images present in the 'order' file, and add the rest
    images = set(glob.glob(f"{gdir}/*.jpg")) - set(images)

    for f in sorted(images):
        img = Image.open(f)
        tmp.append({"src": f, "width": img.width, "height": img.height})

    galleries[gname] = tmp

print("const data =")
print(json.dumps(galleries, indent=2))
print(";")
print("export { data };")
